# Leaflet Map

Customize Leaflet Map for the Philippines site demo here : https://rossvincentdiaz12.gitlab.io/Leaflet-Map

### Prerequisites

Leaflet

### Installing

* [LEAFLET download](https://leafletjs.com/download.html)


## Built With

* [LEAFLET](https://leafletjs.com/) 

## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/rossvincentdiaz12/Leaflet-Map) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [LEAFLET](https://github.com/Leaflet/Leaflet) 
